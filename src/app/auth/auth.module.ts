import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ContactComponent } from './components/contact/contact.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { GiftCertificatesComponent } from './components/gift-certificates/gift-certificates.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ LoginComponent, SignUpComponent, ContactComponent, MyAccountComponent, GiftCertificatesComponent ],
})
export class AuthModule { }