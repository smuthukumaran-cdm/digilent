import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/store/header/header.component';
import { SideNavComponent } from './layout/store/side-nav/side-nav.component';
import { LoginComponent } from './auth/components/login/login.component';
import { SignUpComponent } from './auth/components/sign-up/sign-up.component';
import { ContactComponent } from './auth/components/contact/contact.component';
import { MyAccountComponent } from './auth/components/my-account/my-account.component';
import { GiftCertificatesComponent } from './auth/components/gift-certificates/gift-certificates.component';
import { FooterComponent } from './layout/store/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    ContactComponent,
    MyAccountComponent,
    GiftCertificatesComponent,
    HeaderComponent,
    FooterComponent,
    SideNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
